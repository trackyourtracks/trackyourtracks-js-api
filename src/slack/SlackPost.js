function SlackPost (){
  
  this.setTarget = function(target){
    this.channel = target;
  };

  this.setName = function(name){
    this.username = name;
  };

  this.setIcon = function(icon){
    this.icon_url = icon;
  };

  this.setMessage = function(message){
    this.text = message;
  };

}
