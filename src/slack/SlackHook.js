function SlackHook(url) {

  this.url = url;

  this.post = function(post) {
    var xhr = new XMLHttpRequest();
    xhr.open("POST", this.url);
    xhr.send(JSON.stringify(post));
  };

}
